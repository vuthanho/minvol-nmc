# Nonnegative Matrix Completion with Minimum-volume

## WARNING: This code was under development and might not be updated

## Recommended installation

* Start Julia
* Type `]` to enter `Pkg` mode
* Add my registry: `registry add https://gitlab.com/vuthanho/vuthanhoregistry`
* Install the PartMinvol package: `add PartMinvol`
* Leave `Pkg` mode with backspace ⌫
* `using PartMinvol`