using PartMinvol
using SparseArrays
using LinearAlgebra
using Combinatorics
using PlotlyLight
using Statistics
using DelimitedFiles

include("util.jl")

m,n = 200,200
rs = 5:10
missing_rates = 0.8:0.01:0.9
runs = 20

# constraints = [ (name="NMF", projW! = nnProj!, projH! = nnProj!, minvol=false),
#                 (name="Minvol", lambda_W=1),
#                 (name="auto_Minvol", lambda_W=1,  auto_lambda=true),
#                 (name="auto_Minvol_penFro", lambda_W=1, gamma=0.1, auto_lambda=true, projW! = nnProj!)]

constraints = [ (name="auto_Minvol_penFro", lambda_W=1, gamma=0.01, auto_lambda=true, projW! = nnProj!)]

common_settings = (inneriter=20,maxiter=50)

RMSEs = Dict{String,AbstractArray}()
Angles = Dict{String,AbstractArray}()
for c in constraints
    RMSEs[c[:name]] = Array{Float64,3}(undef,length(rs),length(missing_rates),runs)
    Angles[c[:name]] = Array{Float64,3}(undef,length(rs),length(missing_rates),runs)
end

for run in 1:runs
    for (i,r) in enumerate(rs)
        X,W,H = gen_X(m,n,r,0.2)
        W0 = rand(m,r)
        H0 = rand(r,n)
        for (j,missing_rate) in enumerate(missing_rates)
            Xtrain,Mtrain,Mtest = gen_missing(X,missing_rate)
            cfg0 = partminvol(X=Xtrain,r=r,W=copy(W0),H=copy(H0),inneriter=1,maxiter=500,minvol=false,projW! = simplexProj!)

            println("\n$(repeat('-',20)) run $run | r $r | missing rate $missing_rate $(repeat('-',20))\nInitialization | train rmse $(round(rmse(X[Mtrain],(cfg0.W*cfg0.H)[Mtrain]),sigdigits=3)) | test rmse $(round(rmse(X[Mtest],(cfg0.W*cfg0.H)[Mtest]),sigdigits=3)) | subspace $(round(subspace(W,cfg0.W),sigdigits=3))")

            for constraint in constraints
                cfg = partminvol(;X=Xtrain,r=r,W=copy(cfg0.W),H=copy(cfg0.H),common_settings...,constraint...)
                # Saving results
                test_rmse = rmse(X[Mtest],(cfg.W*cfg.H)[Mtest])
                angle = subspace(W,cfg.W)
                RMSEs[constraint[:name]][i,j,run] = test_rmse
                Angles[constraint[:name]][i,j,run] = angle
                println("$(constraint[:name]) | train rmse $(round(rmse(X[Mtrain],(cfg.W*cfg.H)[Mtrain]),sigdigits=3)) | test rmse $(round(test_rmse,sigdigits=3)) | subspace $(round(angle,sigdigits=3))")
            end
        end
    end
end

mean_RMSEs  = Dict{String,AbstractArray}()
std_RMSEs   = Dict{String,AbstractArray}()
mean_Angles = Dict{String,AbstractArray}()
std_Angles  = Dict{String,AbstractArray}()
for c in constraints
    mean_RMSEs[c[:name]]    = reverse(dropdims(mean(RMSEs[c[:name]],dims=3),dims=3),dims=1)
    std_RMSEs[c[:name]]     = reverse(dropdims(std(RMSEs[c[:name]],dims=3),dims=3),dims=1)
    mean_Angles[c[:name]]   = reverse(dropdims(mean(Angles[c[:name]],dims=3),dims=3),dims=1)
    std_Angles[c[:name]]    = reverse(dropdims(std(Angles[c[:name]],dims=3),dims=3),dims=1)

    string_mean_rmse 	= string.(round.(mean_RMSEs[c[:name]],digits=2))
    string_mean_angle 	= string.(round.(mean_Angles[c[:name]],digits=2))
    string_std_rmse 	= string.(round.(std_RMSEs[c[:name]],digits=2))
    string_std_angle 	= string.(round.(std_Angles[c[:name]],digits=2))

    string_mean_rmse[:,1]   =   "{".*   string_mean_rmse[:,1]
    string_mean_angle[:,1]  =   "{".*  string_mean_angle[:,1]
    string_std_rmse[:,1]    =   "{".*    string_std_rmse[:,1]
    string_std_angle[:,1]   =   "{".*   string_std_angle[:,1]

    string_mean_rmse[:,end]   =   string_mean_rmse[:,end] .*"}"
    string_mean_angle[:,end]  =   string_mean_angle[:,end].*"}"
    string_std_rmse[:,end]    =   string_std_rmse[:,end]  .*"}"
    string_std_angle[:,end]   =   string_std_angle[:,end] .*"}"

    string_mean_rmse    .*= ","
    string_mean_angle   .*= "," 
    string_std_rmse     .*= ","
    string_std_angle    .*= ","
    
    writedlm("xp/missing_rate_res/mean_rmse_"	*c[:name]*".txt",string_mean_rmse)
    writedlm("xp/missing_rate_res/mean_angle_"	*c[:name]*".txt",string_mean_angle)
    writedlm("xp/missing_rate_res/std_rmse_"	*c[:name]*".txt",string_std_rmse)
    writedlm("xp/missing_rate_res/std_angle_"	*c[:name]*".txt",string_std_angle)
end