# Auxiliary functions

using Statistics
using LinearAlgebra
using Hungarian

function gen_X(m,n,r,H_density=0.3)
    W = rand(m,r)
    H = Array{Float64}(undef, r, 0)
    for _ in 1:n
        H_j = 0
        while iszero(H_j)
            H_j = sprand(r,H_density)
        end
        H = hcat(H,Vector(H_j))
    end
    X = W*H
    μX = mean(X)
    return X./μX,W./sqrt(μX),H./sqrt(μX)
end

function gen_missing(X,missing_rate)
    m,n = size(X)
    Mtrain = Array{Bool}(undef, m, 0)
    for _ in 1:n
        Mtrain_j = false
        while !reduce(|,Mtrain_j)
            Mtrain_j = sprand(Bool,m,1-missing_rate)
        end
        Mtrain = hcat(Mtrain,Mtrain_j)
    end
    rows,cols,_ = findnz(Mtrain)
    Xtrain = sparse(rows,cols,X[Mtrain],m,n)
    Mtest = .!Mtrain
    dropzeros!(Mtest)
    return Xtrain,Mtrain,Mtest
end

function rmse(x,y)
    return norm(x-y)/sqrt(prod(size(x)))
end

# Function similar to Matlab's logspace
logrange(x1, x2, n) = collect(10^y for y in range(log10(x1), log10(x2), length=n))

# Returns a permutation of columns of W such that it is as close as possible to W0
function closestperm(W, W0)
    normW0 = copy(W0)
    for col in eachcol(normW0)
        col ./= norm(col)
    end
    normW = copy(W)
    for col in eachcol(normW)
        col ./= norm(col)
    end
    costmat = -normW0' * normW
    # Safety
    if iszero(length(costmat)) || !isnothing(findfirst(isnan, costmat))
        return zeros(size(W))
    end
    permutation, error = hungarian(costmat)
    filter!(!iszero, permutation)
    return permutation
end

# Compute the mean removed spectral angle (MRSA) between two vectors
function mrsa(x, y)
    x = x .- mean(x)
    y = y .- mean(y)
    score = 0
    if (norm(x) > 0) && (norm(y) > 0)
        x = x / norm(x)
        y = y / norm(y)
        val = clamp(x' * y, -1.0, 1.0)
        score = abs(acos(val)) * 100 / π
    end
    return score
end

# Compute the maximum MRSA between two sets of vectors
function maxmrsa(W, W0)
    if length(W) < length(W0)
        return 1.0
    end
    permW = closestperm(W, W0)
    maxangle = 0.0
    for j in axes(W0, 2)
        curangle = mrsa(W[:,permW[j]], W0[:,j])
        maxangle = max(maxangle, curangle)
    end
    if isnan(maxangle)
        maxangle = 1.0
    end
    return maxangle
end

# Compute the mean MRSA between two sets of vectors
function meanmrsa(W, W0)
    if length(W) < length(W0)
        return 1.0
    end
    permW = closestperm(W, W0)
    angles = zeros(size(W0, 2))
    for j in axes(W0, 2)
        angles[j] = mrsa(W[:,permW[j]], W0[:,j])
    end
    return mean(angles)
end

#   Angle between subspaces.
#   subspace(A,B) finds the angle (in degrees by default, specify subspace(A,B;rad=true) to output in radians) between two subspaces specified by the
#   columns of A and B.
#
#   If the angle is small, the two spaces are nearly linearly dependent.
#   In a physical experiment described by some observations A, and a second
#   realization of the experiment described by B, SUBSPACE(A,B) gives a
#   measure of the amount of new information afforded by the second
#   experiment not associated with statistical errors of fluctuations.
#
#   Class support for inputs A, B:
#      see support of svd

#   The algorithm used here ensures that small angles are computed
#   accurately, and it allows subspaces of different dimensions following
#   the definition in [2]. The first issue is crucial.  The second issue is
#   not so important; but since the definition from [2] coincides with the
#   standard definition when the dimensions are equal, there should be no
#   confusion - and subspaces with different dimensions may arise in
#   problems where the dimension is computed as the numerical rank of some
#   inaccurate matrix.

#   References:
#   [1] A. Bjorck & G. Golub, Numerical methods for computing
#       angles between linear subspaces, Math. Comp. 27 (1973),
#       pp. 579-594.
#   [2] P.-A. Wedin, On angles between subspaces of a finite
#       dimensional inner product space, in B. Kagstrom & A. Ruhe (Eds.),
#       Matrix Pencils, Lecture Notes in Mathematics 973, Springer, 1983,
#       pp. 263-285.
function subspace(A,B;rad::Bool=false)
    #Check rank and swap
    if size(A,2) < size(B,2)
       return subspace(B,A;rad=rad)
    end
    # Compute orthonormal bases, using SVD in "orth" to avoid problems
    # when A and/or B is nearly rank deficient.
    A = svd(A).U
    B = svd(B).U
    # Compute the projection according to [1].
    B = B - A*(A'*B)
    # Make sure it's magnitude is less than 1.
    if rad
        return asin(min(1,opnorm(B)))
    else
        return asind(min(1,opnorm(B)))
    end
end
    