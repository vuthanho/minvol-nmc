function pgd!(cfg::Config)
    _PGD_W!(cfg) # Update of W
    _precomp_afterW!(cfg)
    _PGD_H!(cfg) # Update of H
    _precomp_afterH!(cfg)
end

function _init_pgd!(cfg)
    cfg._update! = pgd!
    return nothing
end

"""
`_PGD_W!(cfg)` performs `cfg.inneriter` PGD updates of `W`. If `cfg.lambda_W != 0`, the volume of `W` is penalized with a weight equal to `cfg.lambda_W`.
"""
function _PGD_W!(cfg)
    W,H,HHt,Diff,X,rows,cols = cfg.W,cfg.H,cfg.HHt,cfg.Diff,cfg.X,cfg.rows,cfg.cols
    # if !cfg.minvol L=opnorm(HHt+cfg.gamma*I) end 
    if !cfg.minvol L=opnorm(HHt) end 
    Ht = copy(H')
    for iter in 1:cfg.inneriter
        if cfg.extra
            α0 = cfg.α[1]
            cfg.α[1] = 0.5*(1+sqrt(1+4*α0^2))
            β = (α0-1)/cfg.α[1]
            extrapolate!(cfg.Wextra,W,cfg.Wold,β)
            copy!(W,cfg.Wextra)
        end
        partXmWH!(Diff,X.nzval,W',H,rows,cols)
        if cfg.minvol
            P,λ = cfg.P,cfg.lambda_W
            mul!(P,W',W)
            adddiag!(P,cfg.delta_W)
            inv!(P)
            L = opnorm(HHt + λ*P)
            W .= W .+ (Diff*H' .- λ.*W*P) ./ L
        else
            # W .= (1-cfg.gamma/L).*W .+ Diff*H' ./ L
            # mul!(W,Diff,Ht,1/L,1-cfg.gamma/L)
            mul!(W,Diff,Ht,1/L,1)
        end
        cfg.projW!(W)
    end
    return nothing
end

"""
`_PGD_H!(cfg)` performs `cfg.inneriter` PGD updates of `H`.
"""
function _PGD_H!(cfg)
    W,H,WtW,Diff,X,rows,cols = cfg.W,cfg.H,cfg.WtW,cfg.Diff,cfg.X,cfg.rows,cfg.cols
    L=norm(WtW+cfg.gamma*I)
    Wt = copy(W')
    for iter in 1:cfg.inneriter
        if cfg.extra
            α0 = cfg.α[2]
            cfg.α[2] = 0.5*(1+sqrt(1+4*α0^2))
            β = (α0-1)/cfg.α[2]
            extrapolate!(cfg.Hextra,H,cfg.Hold,β)
            copy!(H,cfg.Hextra)
        end
        partXmWH!(Diff,X.nzval,W',H,rows,cols)
        # H .= (1-cfg.gamma/L).*H .+ W'*Diff ./ L
        mul!(H,Wt,Diff,1/L,1-cfg.gamma/L)
        cfg.projH!(H)
    end
    return nothing
end
