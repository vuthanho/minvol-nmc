function ianls!(cfg::Config)
    _IANLS_W!(cfg) # Update of W
    _precomp_afterW!(cfg)
    _IANLS_H!(cfg) # Update of H
    _precomp_afterH!(cfg)
end

function _init_ianls!(cfg)
    cfg._update! = ianls!
    cfg.projW! = nnProj!
    cfg.projH! = nnProj!
    if isempty_cfg(cfg.alpha_0) cfg.alpha_0 = 0.05 end
    if isempty_cfg(cfg.nu) cfg.nu = 1 end
    cfg.freq_W = [cfg.gamma*nnz(cfg.X[i,:])^cfg.nu for i in axes(cfg.X,1)]
    cfg.opnorm_freq_W = maximum(cfg.freq_W)
    cfg.freq_H = [cfg.gamma*nnz(cfg.X[:,j])^cfg.nu for j in axes(cfg.X,2)]
    cfg.opnorm_freq_H = maximum(cfg.freq_H)
    return nothing
end

"""
`_IANLS_W!(cfg)` performs `cfg.inneriter` IANLS updates of `W`. If `cfg.lambda_W != 0`, the volume of `W` is penalized with a weight equal to `cfg.lambda_W`.
"""
function _IANLS_W!(cfg)
    W,H,HHt,Diff,X,rows,cols = cfg.W,cfg.H,cfg.HHt,cfg.Diff,cfg.X,cfg.rows,cfg.cols
    L=(1+cfg.alpha_0)*opnorm(HHt)+cfg.opnorm_freq_W
    for iter in 1:cfg.inneriter
        if cfg.extra
            α0 = cfg.α[1]
            cfg.α[1] = 0.5*(1+sqrt(1+4*α0^2))
            β = (α0-1)/cfg.α[1]
            extrapolate!(cfg.Wextra,W,cfg.Wold,β)
            copy!(W,cfg.Wextra)
        end
        partXmWH!(Diff,X.nzval,W',H,rows,cols)
        W .= W .+ (Diff*H' .- cfg.alpha_0.*W*HHt .- cfg.freq_W.*W) ./ L
        cfg.projW!(W)
    end
    return nothing
end

"""
`_IANLS_H!(cfg)` performs `cfg.inneriter` IANLS updates of `H`.
"""
function _IANLS_H!(cfg)
    W,H,WtW,Diff,X,rows,cols = cfg.W,cfg.H,cfg.WtW,cfg.Diff,cfg.X,cfg.rows,cfg.cols
    L=(1+cfg.alpha_0)*opnorm(WtW)+cfg.opnorm_freq_H
    for iter in 1:cfg.inneriter
        if cfg.extra
            α0 = cfg.α[2]
            cfg.α[2] = 0.5*(1+sqrt(1+4*α0^2))
            β = (α0-1)/cfg.α[2]
            extrapolate!(cfg.Hextra,H,cfg.Hold,β)
            copy!(H,cfg.Hextra)
        end
        partXmWH!(Diff,X.nzval,W',H,rows,cols)
        H .= H .+ (W'*Diff .- cfg.alpha_0.*WtW*H .- H.*cfg.freq_H') ./ L
        cfg.projH!(H)
    end
    return nothing
end
