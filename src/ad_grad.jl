function ad_grad!(cfg::Config)
    _ad_grad_W!(cfg) # Update of W
    _precomp_afterW!(cfg)
    _PGD_H!(cfg) # Update of H
    _precomp_afterH!(cfg)
end

function _init_ad_grad!(cfg)
    _init_ad_grad_W!(cfg)
    _init_ad_grad_H!(cfg)
    cfg._update! = ad_grad!
    return nothing
end

function _init_ad_grad_W!(cfg)
    T=cfg.T
    cfg.la_W_k = fill(1/opnorm(cfg.HHt),2) #ones(T,2)
    cfg.La_W_k = 1 ./ cfg.la_W_k #ones(T,2)
    cfg.the_W = T(1e9)
    cfg.The_W = T(1e9)
    cfg.Wold = copy(cfg.W)
    cfg.Wextraold = copy(cfg.Wold)
    cfg.grad_W = zero(cfg.W)
    partXmWH!(cfg.Diff,cfg.X.nzval,cfg.W',cfg.H,cfg.rows,cfg.cols)
    cfg.grad_Wold = -cfg.Diff*cfg.H' + cfg.lambda_W*cfg.W*inv(cfg.W'*cfg.W + cfg.delta_W*I)
    if !isempty_cfg(cfg.gamma) cfg.grad_Wold .+= cfg.gamma .* cfg.W end
    cfg.W .-= T(1e-6).*cfg.grad_Wold
    cfg.Wextra = copy(cfg.W)
    cfg.projW!(cfg.W)
    _precomp_afterW!(cfg)
end

function _init_ad_grad_H!(cfg)
    T=cfg.T
    cfg.la_H_k = fill(1/opnorm(cfg.WtW),2) #ones(T,2)
    cfg.La_H_k = 1 ./ cfg.la_H_k #ones(T,2)
    cfg.the_H = T(1e9)
    cfg.The_H = T(1e9)
    cfg.Hold = copy(cfg.H)
    cfg.Hextraold = copy(cfg.Hold)
    cfg.grad_H = zero(cfg.H)
    partXmWH!(cfg.Diff,cfg.X.nzval,cfg.W',cfg.H,cfg.rows,cfg.cols)
    cfg.grad_Hold = -cfg.W'*cfg.Diff
    if !isempty_cfg(cfg.gamma) cfg.grad_Hold .+= cfg.gamma .* cfg.H end
    cfg.H .-= T(1e-6).*cfg.grad_Hold
    cfg.projH!(cfg.H)
    cfg.Hextra = copy(cfg.H)
    _precomp_afterH!(cfg)
end

function _ad_grad_W!(cfg)
    W,H,Diff,X,rows,cols = cfg.W,cfg.H,cfg.Diff,cfg.X,cfg.rows,cfg.cols
    for iter in 1:cfg.inneriter
        partXmWH!(Diff,X.nzval,cfg.Wextra',H,rows,cols)
        mul!(cfg.grad_W,Diff,H',-1,0)
        if !isempty_cfg(cfg.gamma) cfg.grad_W .+= cfg.gamma .* cfg.Wextra end
        if cfg.minvol
            P,λ = cfg.P,cfg.lambda_W
            mul!(P,cfg.Wextra',cfg.Wextra)
            adddiag!(P,cfg.delta_W)
            inv!(P)
            mul!(cfg.grad_W,cfg.Wextra,P,λ,1)
        end
        cfg.Wextraold .-= cfg.Wextra
        norm_W = norm(cfg.Wextraold)
        cfg.grad_Wold .-= cfg.grad_W
        norm_grad = norm(cfg.grad_Wold)
        cfg.la_W_k[2] = min(sqrt(1+cfg.the_W/2)*cfg.la_W_k[1], 0.5*safe_division(norm_W,norm_grad))
        cfg.La_W_k[2] = min(sqrt(1+cfg.The_W/2)*cfg.La_W_k[1], 0.5/safe_division(norm_W,norm_grad))
        cfg.the_W = cfg.la_W_k[2]/cfg.la_W_k[1]
        cfg.The_W = cfg.La_W_k[2]/cfg.La_W_k[1]
        W .= cfg.Wextra .- cfg.la_W_k[2] .* cfg.grad_W
        cfg.projW!(W)
        t = sqrt(cfg.La_W_k[2]*cfg.la_W_k[2])
        β = (1-t)/(1+t)
        copy!(cfg.Wextraold,cfg.Wextra)
        extrapolate!(cfg.Wextra,W,cfg.Wold,β)
        cfg.la_W_k[1] = cfg.la_W_k[2]
        cfg.La_W_k[1] = cfg.La_W_k[2]
        copy!(cfg.grad_Wold,cfg.grad_W)
    end
    return nothing
end

function _ad_grad_H!(cfg)
    W,H,Diff,X,rows,cols = cfg.W,cfg.H,cfg.Diff,cfg.X,cfg.rows,cfg.cols
    for iter in 1:cfg.inneriter
        partXmWH!(Diff,X.nzval,W',cfg.Hextra,rows,cols)
        mul!(cfg.grad_H,W',Diff,-1,0)
        if !isempty_cfg(cfg.gamma) cfg.grad_H .+= cfg.gamma .* cfg.Hextra end
        cfg.Hextraold .-= cfg.Hextra
        norm_H = norm(cfg.Hextraold)
        cfg.grad_Hold .-= cfg.grad_H
        norm_grad = norm(cfg.grad_Hold)
        cfg.la_H_k[2] = min(sqrt(1+cfg.the_H/2)*cfg.la_H_k[1], 0.5*safe_division(norm_H,norm_grad))
        cfg.La_H_k[2] = min(sqrt(1+cfg.The_H/2)*cfg.La_H_k[1], 0.5/safe_division(norm_H,norm_grad))
        cfg.the_H = cfg.la_H_k[2]/cfg.la_H_k[1]
        cfg.The_H = cfg.La_H_k[2]/cfg.La_H_k[1]
        H .= cfg.Hextra .- cfg.la_H_k[2] .* cfg.grad_H
        cfg.projH!(H)
        t = sqrt(cfg.La_H_k[2]*cfg.la_H_k[2])
        β = (1-t)/(1+t)
        copy!(cfg.Hextraold,cfg.Hextra)
        extrapolate!(cfg.Hextra,H,cfg.Hold,β)
        cfg.la_H_k[1] = cfg.la_H_k[2]
        cfg.La_H_k[1] = cfg.La_H_k[2]
        copy!(cfg.grad_Hold,cfg.grad_H)
    end
    return nothing
end

function safe_division(x::T,y::T) where T
    return !iszero(y) ? exp(log(x)-log(y)) : T(1e16) 
end