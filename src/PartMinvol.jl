module PartMinvol

using PrecompileTools
using EasyConfig
using Bumper
using StrideArrays
using LinearAlgebra
using SparseArrays
using MKLSparse
using MKL
using ProgressMeter

export Config, partminvol, nnProj!, simplexProj!

include("pgd.jl")
include("ad_grad.jl")
include("ianls.jl")

function isempty_cfg(c)
    c isa Config && isempty(c)
end

function init_partminvol_cfg!(cfg::Config)
    @assert !isempty_cfg(cfg.X) && !isempty_cfg(cfg.r)
    if !issparse(cfg.X) cfg.X = sparse(cfg.X) end
    m,n = size(cfg.X)
    cfg.Diff = copy(cfg.X)
    cfg.rows,cfg.cols,_ = findnz(cfg.X)
    r = cfg.r
    T = eltype(cfg.X)
    cfg.T = T
    # Default values
    if isempty_cfg(cfg.alg) cfg.alg = :pgd end
    if isempty_cfg(cfg.inneriter) cfg.inneriter = 10 end
    if isempty_cfg(cfg.maxiter) cfg.maxiter = 500 end
    if isempty_cfg(cfg.extra) cfg.extra = true end
    if isempty_cfg(cfg.verbose) cfg.verbose = false end
    if isempty_cfg(cfg.benchmark) cfg.benchmark = true end
    if isempty_cfg(cfg.true_parameters) cfg.true_parameters = false end
    if isempty_cfg(cfg.lambda_W) cfg.lambda_W = T(1) end
    if isempty_cfg(cfg.gamma) cfg.gamma = T(0) end
    if isempty_cfg(cfg.minvol) cfg.minvol = iszero(cfg.lambda_W) ? false : true end
    if !cfg.minvol cfg.lambda_W = T(0) end
    if isempty_cfg(cfg.auto_lambda) cfg.auto_lambda = false end
    if !isempty_cfg(cfg.delta) cfg.delta_W = cfg.delta end
    if isempty_cfg(cfg.delta_W) cfg.delta_W = T(1) end
    if !isa(cfg.projW!,Function) cfg.projW! = simplexProj! end
    if !isa(cfg.projH!,Function) cfg.projH! = nnProj! end
    # Initialization if needed
    if isempty_cfg(cfg.H) cfg.H = rand(T,r,n) end
    if isempty_cfg(cfg.W) cfg.W = rand(T,r,m)' end
    # Computing Diff for the first time
    partXmWH!(cfg.Diff,cfg.X.nzval,cfg.W',cfg.H,cfg.rows,cfg.cols)
    if !isempty_cfg(cfg.lg)
        D = sqrt(2/(log(1+cfg.delta_W)-log(cfg.delta_W)))*norm.(eachrow(cfg.H)).^(-1)
        cfg.W ./= D'
        cfg.H .*= D
        cfg.lg *= max(norm(cfg.Diff)^2,T(1e-6))/(logdet(cfg.W'*cfg.W+cfg.delta_W*I)/(log(1+cfg.delta_W)-log(cfg.delta_W))+0.5norm(cfg.H)^2)
        cfg.lambda_W=2*cfg.lg/(log(1+cfg.delta_W)-log(cfg.delta_W))
        cfg.gamma = cfg.lg
        cfg.true_parameters = true
    end
    # scaling if needed
    if !iszero(cfg.lambda_W) && !iszero(cfg.gamma)  
        D = sqrt(cfg.lambda_W/cfg.gamma)*norm.(eachrow(cfg.H)).^(-1)
        cfg.W ./= D'
        cfg.H .*= D
    end
    # if !(cfg.W isa Adjoint) cfg.W = copy(cfg.W')' end
    # if !(cfg.H isa Adjoint) cfg.H = copy(cfg.H')' end
    # Preallocation of the error vectors
    if cfg.auto_lambda && !cfg.benchmark
        @info "Switching `benchmark` to `true` anyway because `auto_lambda` has been set to `true`"
        cfg.benchmark = true
    end
    if cfg.benchmark
        cfg.err_recon = Vector{T}(undef,cfg.maxiter+1)
        # cfg.err_volH = Vector{T}(undef,cfg.maxiter+1)
        cfg.err_volW = Vector{T}(undef,cfg.maxiter+1)
        cfg.err = Vector{T}(undef,cfg.maxiter+1)
    end
    # if cfg.projH! == simplexProj! 
    #     n1H_col = sum(cfg.H,dims=1)
    #     cfg.H ./= n1H_col
    # end
    # if cfg.projW! == simplexProj! 
    #     n1W_col = sum(cfg.W,dims=1)
    #     cfg.W ./= n1W_col
    # end
    cfg.nX2 = norm(cfg.X)^2
    cfg.WtW = cfg.W'*cfg.W
    cfg.HHt = cfg.H*cfg.H'
    # Needs to be updated to deal with missing values
    # # Scaling to optimize the initialization
    # scaling = diag_opti(cfg.X,cfg.W,cfg.H)
    # cfg.W .*= scaling'
    # Preallocation and precomputing specific to the extrapolated gradient descent
    if cfg.extra
        cfg.α = ones(T,2)
        cfg.Hold = copy(cfg.H)
        cfg.Hextra = similar(cfg.H)
        cfg.Wold = copy(cfg.W)
        cfg.Wextra = similar(cfg.W)
    end
    # Warm start
    if !isempty_cfg(cfg.warm_start) && (cfg.warm_start isa Int ? true : cfg.warm_start)
        maxiter = cfg.warm_start isa Int ? cfg.warm_start : 50
        minvol = cfg.minvol
        cfg.minvol = false
        for iter in 1:maxiter
            pgd!(cfg)
        end
        if cfg.extra
            cfg.α = ones(T,2)
            cfg.Hold = copy(cfg.H)
            cfg.Hextra = similar(cfg.H)
            cfg.Wold = copy(cfg.W)
            cfg.Wextra = similar(cfg.W)
        end
        cfg.minvol = minvol
    end
    # Rescaling lambda
    if !cfg.true_parameters
        cfg.lambda_W = cfg.lambda_W*max(norm(cfg.Diff)^2,T(1e-6))/abs(vol_W(cfg))
        cfg.gamma = cfg.gamma*max(norm(cfg.Diff)^2,T(1e-6))/norm(cfg.H)^2
    end
    cfg.lambda_W0 = cfg.lambda_W
    cfg.gamma0 = cfg.gamma
    # prealloc for minvol
    cfg.P = Matrix{T}(undef,r,r)
    # Preallocation and precomputing specific to each alg
    getfield(PartMinvol, Symbol(:_init_,cfg.alg,:!))(cfg)
    return cfg
end

function partminvol(;kws...)
    return partminvol!(Config(kws...))
end

function partminvol!(cfg::Config)
    init_partminvol_cfg!(cfg::Config)
    if cfg.benchmark 
        cfg.err_recon[1] = norm(cfg.Diff)^2
        cfg.err_volW[1] = vol_W(cfg)
        cfg.err[1] = cfg.err_recon[1] + cfg.lambda_W*cfg.err_volW[1] + cfg.gamma*norm(cfg.H)^2
    end
    if cfg.verbose
        pbar = Progress(cfg.maxiter)
    end
    for iter in 1:cfg.maxiter
        cfg._update!(cfg)
        if cfg.benchmark
            cfg.iter = iter
            partXmWH!(cfg.Diff,cfg.X.nzval,cfg.W',cfg.H,cfg.rows,cfg.cols)
            cfg.err_recon[iter+1] = norm(cfg.Diff)^2
            cfg.err_volW[iter+1] = vol_W(cfg)
            cfg.err[iter+1] = cfg.err_recon[iter+1] + cfg.lambda_W*cfg.err_volW[iter+1] + cfg.gamma*norm(cfg.H)^2
            if isnan(cfg.err[iter+1])
                if cfg.verbose @warn "NaN detected. Stopping the algorithm earlier at iter=$(cfg.iter)." end
                break
            end
        end
        if cfg.verbose next!(pbar) end
        if cfg.auto_lambda && abs(cfg.err[iter+1] - cfg.err[iter])/cfg.nX2<1e-3
            auto_lambda!(cfg)
            # if cfg.verbose println("Changing λ to $(cfg.lambda_H) at iter $(cfg.iter)") end
        end
    end
    if cfg.verbose finish!(pbar) end
    return cfg
end

function vol_W(cfg::Config)
    if !isempty_cfg(cfg.WtW)
        return logdet(cfg.WtW+cfg.delta_W*I)
    else
        return logdet(cfg.W'*cfg.W+cfg.delta_W*I)
    end
end

function auto_lambda!(cfg)
    if cfg.benchmark && !isempty_cfg(cfg.iter)
        root_rk = sqrt(cfg.err_recon[cfg.iter+1] + 0.1)
    else
        root_rk = sqrt(norm(cfg.Diff)^2 + 0.1)
    end
    cfg.lambda_W = 2*root_rk*cfg.lambda_W0
    cfg.gamma = 2*root_rk*cfg.gamma0
end

function _precomp_afterW!(cfg)
    mul!(cfg.WtW,cfg.W',cfg.W)
    return nothing
end

function _precomp_afterH!(cfg)
    mul!(cfg.HHt,cfg.H,cfg.H')
    return nothing
end

function extrapolate!(Aextra,A,Aold,β)
    @. Aextra = (1+β)*A - β*Aold
    copy!(Aold,A)
end

function simplexProj!(y::AbstractArray{T,2},epsilon::T=zero(T)) where T <: AbstractFloat
    if Threads.nthreads() > 1
        Threads.@threads for i in 1:size(y,2)
            simplexProj!(view(y,:,i),epsilon)
        end
    else
        for i in 1:size(y,2)
            simplexProj!(view(y,:,i),epsilon)
        end
    end
    return nothing
end

function simplexProj!(y::AbstractArray{T,1},epsilon::T=zero(T)) where T <: AbstractFloat
    a = 1
    N = size(y,1)
    @no_escape begin
        vtilde = @alloc(Int64,N)
        v = @alloc(Int64,N)
        v[1] = 1
        vlength = 1
        vtildelength = 0
        rho = y[1] - a
        for n in 2:N
            if y[n]>rho
                rho += (y[n]-rho)/(vlength+1)
                if rho>y[n]-a
                    vlength+=1
                    v[vlength] = n
                else
                    vtilde[vtildelength+1:vtildelength+vlength] = v[1:vlength]
                    vtildelength += vlength
                    v[1] = n
                    vlength = 1
                    rho = y[n]-a
                end
            end
        end
        if vtildelength != 0
            for i in 1:vtildelength
                yv = y[vtilde[i]]
                if yv>rho
                    vlength+=1
                    v[vlength] = vtilde[i]
                    rho += (yv-rho)/vlength
                end
            end
        end
        vlengthold = vlength - 1
        while vlengthold != vlength
            vlengthold = vlength
            i=1
            while i<=vlength
                yv = y[v[i]]
                if yv <= rho
                    v[i] = v[vlength]
                    vlength -=1
                    rho+=(rho-yv)/vlength
                else
                    i+=1
                end
            end
        end
        for i in 1:N
            yimrho = y[i]-rho
            if yimrho < epsilon
                y[i] = epsilon
            else
                y[i] = yimrho
            end
        end
    end
    return nothing
end;

function nnProj!(y::AbstractArray{T},epsilon::T=zero(T)) where T<:AbstractFloat
    clamp!(y,epsilon,Inf)
    return nothing
end

# function inv!(X)
#     LinearAlgebra.LAPACK.potrf!('U',X)
#     LinearAlgebra.LAPACK.potri!('U',X)
#     LinearAlgebra.copytri!(X,'U')
# end

function inv!(X)
    X .= inv(X)
    return nothing
end

function adddiag!(A::Matrix, a::Number)
    m, n = size(A)
    m == n || error("A must be square.")
    if a != 0.0
        for i = 1:m
            @inbounds A[i,i] += a
        end
    end
    return A
end

function diag_opti(X,W,H)
    r = size(W,2)
    b = [W[:,i]'*X*H[i,:] for i in 1:r]
    A = (W'*W).*(H*H')
    d = A\b
    return d
end

function partXmWH!( Diff::AbstractSparseMatrix{T},
    V::AbstractVector{T},
    Wt::AbstractMatrix{T},
    H::AbstractMatrix{T},
    rows::Vector{Int64},
    cols::Vector{Int64}) where T <: AbstractFloat

    if Threads.nthreads() > 1
        @views Threads.@threads for k in eachindex(rows)
            @inbounds Diff.nzval[k] = V[k] - dot(Wt[:,rows[k]],H[:,cols[k]])
        end
    else
        @views for k in eachindex(rows)
            @inbounds Diff.nzval[k] = V[k] - dot(Wt[:,rows[k]],H[:,cols[k]])
        end
    end
    return nothing
end

end # module PartMinvol
